﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace book_store.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        [HttpGet]
        [Route("HelloWorld")]
        public string HelloWorld()
        {
            string str = "Hello World!";

            return str;
        }
    }
}
