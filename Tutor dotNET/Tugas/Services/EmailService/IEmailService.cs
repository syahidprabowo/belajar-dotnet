﻿using Tugas.Model;

namespace Tugas.Services.EmailService
{
    public interface IEmailService
    {
        void sendEmail(DailyNotesModel request);
    }
}
