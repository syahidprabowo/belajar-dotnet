﻿using MimeKit;
using MimeKit.Text;
using Tugas.Model;
using MailKit.Net.Smtp;

namespace Tugas.Services.EmailService
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _config;

        public EmailService(IConfiguration config)
        {
            _config = config;
        }

        public void sendEmail(DailyNotesModel request)
        {
            var email = new MimeMessage();
            //email.From.Add(MailboxAddress.Parse("dummyemail3202@gmail.com"));
            //email.To.Add(MailboxAddress.Parse("dummyemail3202@gmail.com"));
            //email.Subject = request.Subject;
            //email.Body = new TextPart(TextFormat.Html) { Text = request.Body };

            email.From.Add(MailboxAddress.Parse("dummyemail3202@gmail.com"));
            email.To.Add(MailboxAddress.Parse(request.emailTo));
            //email.Body = new TextPart(TextFormat.Html) { Text = request.Notes };

            string txt = "<center><h1>Daily Notes</h1></center><ul>";

            if(request.Notes != null)
            {
                foreach (string dnm in request.Notes)
                {
                    string temp = "<li>" + dnm + "</li>";
                    txt += temp;
                }
            }
            txt += "</ul>";

            email.Body = new TextPart(TextFormat.Html) { Text = txt };

            var List = request.Notes;

            using var smtp = new SmtpClient();
            smtp.Connect(_config.GetSection("EmailHost").Value,Convert.ToInt32(_config.GetSection("EmailPort").Value));
            smtp.Authenticate(_config.GetSection("EmailUserName").Value,_config.GetSection("EmailPassword").Value);
            smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}
