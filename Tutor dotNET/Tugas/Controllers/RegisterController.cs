﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using Tugas.Model;
using Tugas.Services.EmailService;

namespace Tugas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private readonly IConfiguration? _config;
        private readonly IEmailService? _emailService;

        public RegisterController(IConfiguration config, IEmailService emailService)
        {
            _config = config;
            _emailService = emailService;
        }

        [HttpPost]
        [Route("User")]
        public IActionResult Register(UserModelAuth users)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
                {
                    string query = "INSERT INTO Users VALUES ('" + users.userName + "','" + users.Password + "','" + users.Email + "','" + users.Role + "','" + users.surName + "','" + users.givenName + "') ";
                    con.Open();
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }

                EmailDTO request = new EmailDTO();
                request.Subject = "Verify Link";
                request.To = "drxeno99@gmail.com";
                request.Body = "<a>Hello Guys</a>";
                //_emailService.sendEmail(request);

                return Ok("Successfully Register and Sending Email");
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
    }
}
