﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tugas.Model;
using Tugas.Services.EmailService;

namespace Tugas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailService _emailService;

        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;
        }

        [HttpPost]
        [Route("DailyNotes")]
        public IActionResult sendEmail(DailyNotesModel request)
        {
            _emailService.sendEmail(request);

            return Ok(request.Notes);
        }
    }
}
