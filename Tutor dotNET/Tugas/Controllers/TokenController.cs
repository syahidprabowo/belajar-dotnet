﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace Tugas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public TokenController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        [Route("GenerateToken")]
        public ActionResult GenerateToken()
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);


            string jwtToken = "";
                //JwtSecurityToken(_configuration["Jwt:issuer"],
                //_configuration["Jwt:Audience"],
                //null,
                //expires: DateTime.Now.AddMinutes(3),
                //signingCredentials: credentials
                //); ;

            return Ok(jwtToken);
        }

        [HttpGet]
        [Route("ValidateToken")]
        [Authorize (Roles = "Admin")]
        public ActionResult ValidateToken()
        {
            return Ok("Congrats, your token is right!");
        }
    }
}
