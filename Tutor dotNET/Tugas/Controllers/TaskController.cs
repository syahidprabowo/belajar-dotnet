﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using Tugas.Model;

namespace Tugas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public TaskController (IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        [Route("GetUserWithTask")]
        public List<OutputModel> GetUserWithTask(string name)
        {
            List<OutputModel> output = new List<OutputModel>();

            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string queryUser = "SELECT * FROM Users";
                SqlCommand cmd = new SqlCommand(queryUser, conn);

                if (!string.IsNullOrEmpty(name))
                {
                    queryUser = "SELECT * FROM Users WHERE name='@name'";
                    cmd = new SqlCommand(queryUser, conn);
                    cmd.Parameters.AddWithValue("@name", name);
                }

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                DataTable dtUser = new DataTable();
                sqlDataAdapter.Fill(dtUser);

                for (int i = 0; i < dtUser.Rows.Count; i++)
                {
                    OutputModel oModel = new OutputModel();
                    oModel.pk_users_id = Convert.ToInt32(dtUser.Rows[i]["pk_users_id"].ToString());
                    List<TaskModel> listTask = new List<TaskModel>();

                    for (int j = 0; j < dtUser.Rows.Count; j++)
                    {
                        if (dtUser.Rows[i]["pk_users_id"].ToString() == dtUser.Rows[j]["fk_users_id"].ToString())
                        {
                            TaskModel tModel = new TaskModel();
                            tModel.pk_tasks_id = Convert.ToInt32(dtUser.Rows[j]["pk_tasks_id"].ToString());
                            tModel.task_detail = dtUser.Rows[j]["task_detail"].ToString();
                            listTask.Add(tModel);
                        }
                    }
                    oModel.tasks = listTask;
                    output.Add(oModel);
                }
            }

            return output;
        }
    }
}
