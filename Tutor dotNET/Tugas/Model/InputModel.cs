﻿namespace Tugas.Model
{
    public class InputModel
    {
        public string name { get; set; }
        public List<TaskModel> task { get; set; }
    }
}
