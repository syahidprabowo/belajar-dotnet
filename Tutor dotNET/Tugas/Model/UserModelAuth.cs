﻿using Microsoft.AspNetCore.Authentication;

namespace Tugas.Model
{
    public class UserModelAuth
    {
        public string? userName { get; set; }
        public string? Password { get; set; }
        public string? Email { get; set; }
        public string? Role { get; set; }
        public string? surName { get; set; }
        public string? givenName { get; set; }
    }
}
