﻿namespace Tugas.Model
{
    public class OutputModel
    {
        public List<TaskModel>? tasks { get; set; }
        public int? pk_users_id { get; set; }
        public string? name { get; set; }
    }
}
